# Site Closing Animation

Include both files into your html document.

Now, if you click on any a element the site will disappear with a animation and then the new site will be loaded.
Uses CSS3 Animations and JavaScript.
This works in following browsers (tested):
Google Chrome 31.0.1650.48
Internet Explorer 10+
Firefox 24.0