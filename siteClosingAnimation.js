function leaveSite(evt) {
    document.getElementsByTagName('body')[0].className='animate';
    evt.preventDefault();
    that = evt.target;
    document.getElementsByTagName('body')[0].addEventListener('webkitAnimationEnd',  function(){
        document.getElementsByTagName('body')[0].className='hidden';
        window.location.href = that.getAttribute('href');
    });
    document.getElementsByTagName('body')[0].addEventListener('MSAnimationEnd',  function(){
        document.getElementsByTagName('body')[0].className='hidden';
        window.location.href = that.getAttribute('href');
    });
    document.getElementsByTagName('body')[0].addEventListener('mozAnimationEnd',  function(){
        document.getElementsByTagName('body')[0].className='hidden';
        window.location.href = that.getAttribute('href');
    });
    return false;
}

window.onload = function(){
    for(var n = 0; n < document.getElementsByTagName('a').length; n++){
        document.getElementsByTagName('a')[n].addEventListener('click', leaveSite);
    }
};